FROM php:8.2-alpine
MAINTAINER Chris Hallgren <chris@hallgren.net>

ENV XDEBUG_MODE=coverage

RUN apk update && apk upgrade && apk add --no-cache autoconf \
    build-base \
    bash \
    coreutils \
    git \
    openssh \
    icu-dev \
    freetype \
    freetype-dev \
    libpng \
    libpng-dev \
    libjpeg-turbo \
    libjpeg-turbo-dev \
    zlib-dev \
    zlib \
    sqlite \
    sqlite-dev \
    bzip2 \
    bzip2-dev \
    curl \
    curl-dev \
    openssl \
    libmcrypt \
    libmcrypt-dev \
    gnupg \
    gmp \
    gmp-dev \
    libzip-dev \
    findutils \
    libgcc \
    libstdc++ \
    libx11 \
    glib \
    libxrender \
    libxext \
    libintl \
    ttf-dejavu \
    ttf-droid \
    ttf-freefont \
    ttf-liberation \
    python3 \
    py3-pip \
    nodejs \
    npm \
    postgresql16-client \
    postgresql16-dev \
    linux-headers \
    openssl-dev 
RUN mkdir -p ~/.ssh && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
RUN docker-php-ext-configure gd \
    --with-freetype \
    --with-jpeg
RUN CFLAGS="$CFLAGS -D_GNU_SOURCE" docker-php-ext-install intl zip bz2 curl exif ftp pdo sockets gd gmp pdo_pgsql pgsql
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php --install-dir=/bin --filename=composer && php -r "unlink('composer-setup.php');"
RUN pear install PHP_CodeSniffer && pecl install xdebug && docker-php-ext-enable xdebug
COPY --from=madnight/alpine-wkhtmltopdf-builder:0.12.5-alpine3.10 /bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
